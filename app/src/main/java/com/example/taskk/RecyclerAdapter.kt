package com.example.taskk

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.taskk.databinding.ActivityMainBinding
import com.example.taskk.databinding.ItemsBinding
import com.example.taskk.databinding.UserLayoutBinding

class RecyclerAdapter(private val users : MutableList<ItemModel>, private val itemsListener : ItemsInterface) : RecyclerView.Adapter<RecyclerAdapter.ItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemsView = ItemsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(itemsView)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int = users.size

    inner class ItemViewHolder (private val binding : ItemsBinding) : RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: ItemModel
        /**
        private val edit : ImageButton = itemView.findViewById(R.id.editButton)
        private val delete : ImageButton = itemView.findViewById(R.id.deleteButton)*/

        fun bind(){
            model = users[adapterPosition]
            binding.userFirstN.text = model.name
            binding.userLastN.text = model.lastName
            binding.userEmailA.text = model.email
            model.delete?.let { binding.deleteButton.setImageResource(it) }
            model.edit?.let { binding.editButton.setImageResource(it) }
            binding.deleteButton.setOnClickListener {
                itemsListener.userDeleteOnClick(adapterPosition)
            }
            binding.editButton.setOnClickListener {
                itemsListener.userEditOnClick(adapterPosition)
            }

            /*val name : TextView = itemView.findViewById(R.id.name)
            name.text = model.name
            val lastName : TextView = itemView.findViewById(R.id.lastName)
            lastName.text = model.lastName
            val email : TextView = itemView.findViewById(R.id.email)
            email.text = model.email
            delete.setImageResource(R.drawable.ic_cancel)
            edit.setImageResource(R.drawable.ic_edit_button)
            delete.setOnClickListener(this)
            edit.setOnClickListener(this)*/
        }

        /**override fun onClick(v: View?) {
           /** if (delete.isPressed) itemsListener.userDeleteOnClick(adapterPosition)
            if (edit.isPressed) itemsListener.userEditOnClick(adapterPosition)*/
            /**if (v is ImageView) {
                when (v.id) {
                    R.id.deleteButton -> itemsListener.userDeleteOnClick(adapterPosition)
                    R.id.editButton -> itemsListener.userEditOnClick(adapterPosition)
                }
            }*/
        }*/

    }


}