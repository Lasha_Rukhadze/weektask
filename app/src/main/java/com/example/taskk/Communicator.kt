package com.example.taskk

interface Communicator {
    fun respond1 (userUnedited : UserModel)
    fun respond2 (userEdited : UserModel)
    fun respond3 (userAdded : UserModel)
}