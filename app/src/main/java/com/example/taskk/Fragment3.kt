package com.example.taskk

import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.taskk.databinding.Fragment3Binding

class Fragment3 : Fragment() {

    lateinit var binding: Fragment3Binding
    private lateinit var addedUser : UserModel
    lateinit var com : Communicator

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = Fragment3Binding.inflate(inflater, container, false)
        return (binding.root)
    }

    private fun isLetters(string: String): Boolean {
        return string.filter { it in 'A'..'Z' || it in 'a'..'z' }.length == string.length
    }

    private fun check() : Boolean{
        if(binding.firstN.text.isBlank() || binding.lastN.text.isBlank() || binding.email.text.isBlank() || !isLetters(binding.firstN.text.toString()) || !isLetters((binding.lastN).text.toString())
            || !Patterns.EMAIL_ADDRESS.matcher(binding.email.text.toString()).matches()){
            Toast.makeText(activity, "Valid information MUST be entered", Toast.LENGTH_SHORT).show()
            return true
        }
        addedUser = UserModel(binding.firstN.text.toString(), binding.lastN.text.toString(), binding.email.text.toString())
        return false
    }

    private fun add(){
        if(check()) return
        else {
            com.respond3(addedUser)
            findNavController().navigate(R.id.action_fragment3_to_fragment1)
        }
    }
}