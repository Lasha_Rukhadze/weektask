package com.example.taskk

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.taskk.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), Communicator {

    override fun respond1(user: UserModel) {
        var manager : FragmentManager = supportFragmentManager
        var f2 : Fragment2 = manager.findFragmentById(R.id.f2) as Fragment2
        f2.setter(user)
    }

    override fun respond2(editedUser: UserModel) {
        var manager : FragmentManager = supportFragmentManager
        var f1 : Fragment1 = manager.findFragmentById(R.id.f1) as Fragment1
        f1.setter(f1.users, f1.pos, editedUser)
    }

    override fun respond3(userAdded: UserModel) {
        var manager : FragmentManager = supportFragmentManager
        var f1 : Fragment1 = manager.findFragmentById(R.id.f1) as Fragment1
        f1.adder(f1.users, userAdded)
    }


   // lateinit var adapter: RecyclerAdapter
    //private var users = mutableListOf<ItemModel>()

    //val addButton : ImageButton = findViewById(R.id.addButton)
    //private lateinit var user : UserModel
    //lateinit var editedUser : UserModel
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

       // initializer()
    }

   /* private fun editUser(users : MutableList<ItemModel>, position : Int){
        val intent = Intent(this, UserActivity::class.java)
        user = UserModel(users[position].name, users[position].lastName, users[position].email)
        intent.putExtra("User", user)
        startActivityForResult(intent, 1)
        setter(users, position, editedUser)
    }*/

    /**private fun initializer(){
          setData()
          adapter = RecyclerAdapter(users, object : ItemsInterface {

          override fun userDeleteOnClick(position: Int) {
            users.removeAt(position)
            adapter.notifyItemRemoved(position)
          }

          override fun userEditOnClick(position: Int) {
              //editUser(users, position)
              adapter.notifyItemChanged(position)
          }

        })
        /*val recycler : RecyclerView = findViewById(R.id.recycler)
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter*/
    }*/

   /** private fun setData(){
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
    }*/

    /*private fun setter(users : MutableList<ItemModel>, position : Int, editedUser : UserModel) {
        users[position].name = editedUser.userName
        users[position].lastName = editedUser.userLastName
        users[position].email = editedUser.userEmail
    }*/

    /**override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1) {
            if (resultCode == RESULT_OK){
                if (data != null) {
                    editedUser = data.getParcelableExtra("EditedUser")!!
                }
            }
        }
    }*/
}