package com.example.taskk

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.Serializable


data class UserModel(var name : String, var lastName : String, var email : String) : Serializable{

    var userName = name
    var userLastName = lastName
    var userEmail = email
}
