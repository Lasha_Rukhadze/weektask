package com.example.taskk

import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.taskk.databinding.Fragment2Binding

class Fragment2 : Fragment() {

    lateinit var binding: Fragment2Binding
    lateinit var com : Communicator
    lateinit var user : UserModel
    lateinit var editedUser : UserModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = Fragment2Binding.inflate(inflater, container, false)

        binding.saveButton.setOnClickListener {
            save()
        }

        return binding.root
    }

    private fun isLetters(string: String): Boolean {
        return string.filter { it in 'A'..'Z' || it in 'a'..'z' }.length == string.length
    }

    private fun check() : Boolean{
        if(binding.userName.text.isBlank() || binding.userLastName.text.isBlank() || binding.userEmail.text.isBlank() || !isLetters(binding.userName.text.toString()) || !isLetters((binding.userLastName).text.toString())
            || !Patterns.EMAIL_ADDRESS.matcher(binding.userEmail.text.toString()).matches()){
            Toast.makeText(activity, "Valid information MUST be entered", Toast.LENGTH_SHORT).show()
            return true
        }
        editedUser = UserModel(binding.userName.text.toString(), binding.userLastName.text.toString(), binding.userEmail.text.toString())
        return false
    }
    fun setter(user : UserModel){
        this.user = user.copy()
    }

    private fun openFragment1(){
        findNavController().navigate(R.id.action_fragment2_to_fragment1)
    }

    private fun save(){
        if (check()) return
        else {
            com.respond2(editedUser)
            openFragment1()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        com = activity as Communicator
    }
}