package com.example.taskk

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.taskk.databinding.Fragment1Binding
import kotlin.properties.Delegates

class Fragment1 : Fragment() {

    lateinit var binding: Fragment1Binding
    lateinit var adapter: RecyclerAdapter
    lateinit var item : ItemModel
    var users = mutableListOf<ItemModel>()
    var pos by Delegates.notNull<Int>()
    lateinit var com : Communicator
    lateinit var user : UserModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = Fragment1Binding.inflate(inflater, container, false)
        initializer()
        setData()
        binding.addButton.setOnClickListener {
            openFragment3()
        }

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initializer(){

        adapter = RecyclerAdapter(users, object : ItemsInterface {

            override fun userDeleteOnClick(position: Int) {
                users.removeAt(position)
                adapter.notifyItemRemoved(position)
            }

            override fun userEditOnClick(position: Int) {
                pos = position
                //editUser(users, position)
                //adapter.notifyItemChanged(position)
            }

        })
        binding.recycler.layoutManager = LinearLayoutManager(activity)
        binding.recycler.adapter = adapter
    }

    private fun setData(){
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
        users.add(ItemModel("Lasha", "Rukhadze", "lasha.rukhadze.96@gmail.com", R.drawable.ic_cancel, R.drawable.ic_edit_button))
    }

    private fun editUser(users : MutableList<ItemModel>, position : Int){
        user = UserModel(users[position].name, users[position].lastName, users[position].email)
        com.respond1(user)
        openFragment2()
    }

    private fun openFragment2(){
        findNavController().navigate(R.id.action_fragment1_to_fragment23)
    }
    private fun openFragment3(){
        findNavController().navigate(R.id.action_fragment1_to_fragment3)
    }

    fun setter(users : MutableList<ItemModel>, position : Int, editedUser : UserModel) {
        users[position].name = editedUser.userName
        users[position].lastName = editedUser.userLastName
        users[position].email = editedUser.userEmail
    }
    fun adder(users : MutableList<ItemModel>, addedUser : UserModel){
        item.name = addedUser.name
        item.lastName = addedUser.lastName
        item.email = addedUser.email
        users.add(item)
        adapter.notifyDataSetChanged()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        com = activity as Communicator
    }
}