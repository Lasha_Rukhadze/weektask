package com.example.taskk

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

/**abstract class UserActivity : AppCompatActivity() {

    private var userName : EditText = findViewById(R.id.userName)
    private var userLastName : EditText = findViewById(R.id.userLastName)
    private var userEmail : EditText = findViewById(R.id.userLastName)
    private val save : Button = findViewById(R.id.saveButton)
    //abstract var user : UserModel?
    //abstract var editedUser : UserModel?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_layout)

        setter()

        save.setOnClickListener {
            save()
        }
    }

    private fun isLetters(string: String): Boolean {
        return string.filter { it in 'A'..'Z' || it in 'a'..'z' }.length == string.length
    }

    private fun check() : Boolean{
        if(userName.text.isBlank() || userLastName.text.isBlank() || userEmail.text.isBlank() || !isLetters(userName.text.toString()) || !isLetters((userLastName).text.toString())
                || !Patterns.EMAIL_ADDRESS.matcher(userEmail.text.toString()).matches()){
            Toast.makeText(this, "Valid information MUST be entered", Toast.LENGTH_SHORT).show()
            return true
        }
        editedUser = user?.copy()
        return false
    }

    private fun setter(){
        user = intent.getParcelableExtra("User")
        userName.setText(user?.userName)
        userLastName.setText(user?.userLastName)
        userEmail.setText(user?.userEmail)
    }

    private fun save(){
        if (check()) return
        else {
            val returnIntent = Intent()
            returnIntent.putExtra("EditedUser", editedUser)
            setResult(RESULT_OK, returnIntent)
            finish()
        }
    }

}*/