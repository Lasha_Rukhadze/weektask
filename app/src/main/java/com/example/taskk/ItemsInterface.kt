package com.example.taskk

import android.widget.ImageButton


interface ItemsInterface {
        fun userDeleteOnClick(position : Int)
        fun userEditOnClick(position : Int)
    }
